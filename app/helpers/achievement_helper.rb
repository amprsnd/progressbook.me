module AchievementHelper

  def percent(ach)
    all  = User.all.count
    part = UserAchieved.where(achievement_id: ach.id).count
    "#{(part.to_f / all.to_f * 100.0).to_i}%"
  end

  def last_twelve(ach)
    UserAchieved.where(achievement_id: ach.id).order(created_at: :desc).limit(12)
  end

  def and_more(ach)
    more = UserAchieved.where(achievement_id: ach.id).count - 12

    if more > 9999
      ('<div class="and_more tooltipped" data-tooltip="more users">+9999</div>').html_safe
    elsif more < 1
      ''
    else
      ("<div class=\"and_more tooltipped\" data-tooltip=\"more users\">+#{more}</div>").html_safe
    end

  end

end
