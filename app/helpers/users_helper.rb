module UsersHelper

  def user_medals(usr)
    UserMedaled.where(user_id: usr.id).order(created_at: :desc).limit(16)
  end


  def user_achievments(usr)
    UserAchieved.where(user_id: usr.id).order(created_at: :desc).limit(32)
  end


  def user_challenges(usr)
  end

end
