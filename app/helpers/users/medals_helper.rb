module Users::MedalsHelper

  # TODO: get from user medals @medals
  def medaled_date user, med
    date = UserMedaled.find_by(user_id: user.id, medal_id: med.id).created_at
    date.strftime("%B %-d %Y, %H:%M")
  end

end
