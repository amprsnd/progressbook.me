module Users::AchievementsHelper

  # TODO: get from user achievs @achievements
  def achieved_date user, ach
    date = UserAchieved.find_by(user_id: user.id, achievement_id: ach.id).created_at
    date.strftime("%B %-d %Y, %H:%M")
  end

  def proof
    proof = UserAchieved.find_by(user_id: @user.id, achievement_id: @achievement.id )
  end

end
