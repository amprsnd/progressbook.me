module MedalsHelper

  def achievements_count medal, user
    Achievement.joins(:medals, :users)
               .where('medal_id = ? AND user_id = ?', medal, user )
               .count
  end

  def medal_user_percent medal, user
    all  = medal.achievements.count
    #TODO: DRY!
    part = Achievement.joins(:medals, :users)
                      .where('medal_id = ? AND user_id = ?', medal, user )
                      .count
    "#{(part.to_f / all.to_f * 100.0).to_i}%"
  end

  # for single medal (show)
  def medal_percent(medal)
    all  = User.all.count
    part = UserMedaled.where(medal_id: medal.id).count
    "#{(part.to_f / all.to_f * 100.0).to_i}%"
  end

  def medal_last_twelve(medal)
    UserMedaled.where(medal_id: medal.id).order(created_at: :desc).limit(12)
  end

  def medal_and_more(medal)
    more = UserMedaled.where(medal_id: medal.id).count - 12

    if more > 9999
      ('<div class="and_more tooltipped" data-tooltip="more users">+9999</div>').html_safe
    elsif more < 1
      ''
    else
      ("<div class=\"and_more tooltipped\" data-tooltip=\"more users\">+#{more}</div>").html_safe
    end

  end

end
