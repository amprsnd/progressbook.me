class MedalsController < ApplicationController

  before_action :authenticate_user!, only: [:edit, :update]
  layout 'no_sidebar', except: :index

  def index

    @user = current_user

    @cats = Category.all
    @tags = Tag.select("tags.*, COUNT(medals.id) medals_count")
      .joins(:medals)
      .group("tags.id")
      .order("medals_count DESC")
      .limit(24)
    @all_tags = Tag.all


    # TODO: not good... do refactor!
    cat_id = params[:cat]
    tag_id = params[:tag]

    if @cats.exists? cat_id

      @medals = Category.find(cat_id)
        .medals
        .order(id: :desc)
        .page params[:page]

      @cat_id_title = Category.find(cat_id).title

    elsif @all_tags.exists? tag_id

      @medals = Tag.find(tag_id)
        .medals
        .order(id: :desc)
        .page params[:page]
      @cat_id_title = Tag.find(tag_id).name

    else

      @medals = Medal.all
        .order(id: :desc)
        .page params[:page]
      @cat_id_title = ''
      @tag_id_title = ''

    end
    # not good end



    render layout: 'cats_tags'

  end

  def show

    @user = current_user
    @medal = Medal.find params[:id]

  end

  def update

    @user = current_user
    @medal = Medal.find params[:id]

    achievements_count = Achievement.joins(:medals, :users)
                                    .where('medal_id = ? AND user_id = ?', @medal.id, @user.id )
                                    .count

    if achievements_count == @medal.achievements.count
      @medaled = UserMedaled.where(user_id: @user.id, medal_id: @medal.id).first_or_initialize
      unless @medaled.save
        flash[:warning] = 'Sorry, problem... Try again later'
        redirect_to medal_path(@medal.id)
      end
    else
      redirect_to medal_path(@medal.id)
    end
  end

end
