class ProfilesController < ApplicationController

  before_action :authenticate_user!

  def show
    @user = User.find(current_user.id)
  end

  def edit
    @user = User.find(current_user.id)
  end

  def update

    @user = User.find(current_user.id)

    if @user.update_attributes user_params
      flash[:notice] = 'Your profile has been updated'
      redirect_to profile_path
    else
      flash.now[:warning] = 'There were problems when trying to update your profile'
      render :action => :edit
    end
  end

  #delete userpic
  def delete_userpic

    @user = User.find(current_user.id)
    if params[:params][:delete] == 'true'
      @user.userpic = nil
      @user.save
    end
    render :json => {}
  end

  def user_params
    params.require(:user).permit(:name,
                                 :date_of_birth,
                                 :gender,
                                 :country,
                                 :city,
                                 :bio,
                                 :social_link_fb,
                                 :social_link_tw,
                                 :social_link_vk,
                                 :social_link_inst,
                                 :userpic,
                                 :userpic_file_name
    )
  end

end
