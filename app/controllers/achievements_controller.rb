class AchievementsController < ApplicationController

  before_action :authenticate_user!, only: [:edit, :update]
  layout 'no_sidebar', except: :index

  def index

    @user = current_user

    @cats = Category.all
    @tags = Tag.select("tags.*, COUNT(achievements.id) achievements_count")
      .joins(:achievements)
      .group("tags.id")
      .order("achievements_count DESC")
      .limit(24)
    @all_tags = Tag.all


    # TODO: not good... do refactor!
    cat_id = params[:cat]
    tag_id = params[:tag]

    if @cats.exists? cat_id

      @achievements = Category.find(cat_id)
        .achievements
        .order(id: :desc)
        .page params[:page]

      @cat_id_title = Category.find(cat_id).title

    elsif @all_tags.exists? tag_id

      @achievements = Tag.find(tag_id)
        .achievements
        .order(id: :desc)
        .page params[:page]
      @cat_id_title = Tag.find(tag_id).name

    else

      @achievements = Achievement.all
        .order(id: :desc)
        .page params[:page]
      @cat_id_title = ''
      @tag_id_title = ''

    end
    # not good end



    render layout: 'cats_tags'
  end

  def show

    @user = current_user
    @achievement = Achievement.find params[:id]

    #render layout: 'no_sidebar'
  end

  def edit

    @user = current_user
    @achievement = Achievement.find params[:id]

    if @achievement.user_ids.include? @user.id
      flash[:notice] = 'You already have this achievement'
      redirect_to achievements_path
    end

  end

  def update

    #raise user_achievement_params.inspect

    @achievement = Achievement.find params[:id]
    @user = current_user

    @achieved = UserAchieved.where(user_id: @user.id, achievement_id: @achievement.id).first_or_initialize
    @achieved.have_proof = params[:achievement][:user_achieveds][:have_proof]
    @achieved.proof_type = params[:achievement][:user_achieveds][:proof_type]
    @achieved.proof = params[:achievement][:user_achieveds][:proof]
    @achieved.user_id = @user.id
    @achieved.achievement_id = @achievement.id
    @achieved.proof_image = params[:achievement][:user_achieveds][:proof_image]
    #@achieved.proof_image_file_name = params[:achievement][:user_achieveds][:proof_image_file_name]

    unless @achieved.save
      #something wrong...
    end

    #render layout: 'no_sidebar'
  end

  private
    def user_achievement_params
      params.require(:achievement).permit(user_achieveds:[
                                                 :have_proof,
                                                 :proof_type,
                                                 :proof
      ])
    end

  #FIXME: edit/show action 'cancel' button loop
  #<%= Rails.application.routes.recognize_path(request.referer)[:action] %>
end
