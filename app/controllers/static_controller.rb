class StaticController < ApplicationController
  def home

    if current_user

      #show new content
      @user = current_user
      render template: 'static/home_user'

    end

    #else show welcome

  end

  def about
  end

  def contacts
  end

  def presentation
  end
end
