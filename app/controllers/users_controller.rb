class UsersController < ApplicationController

  layout 'no_sidebar', only: :index

  def index
    @users = User.all.order( 'current_sign_in_at DESC' ).page params[:page]
  end

  def show
    @user = User.find params[:id]
  end

end
