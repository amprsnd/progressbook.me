class Dashboard::MedalsController < ApplicationController

  include Dashboard

  def index
    @medals = Medal.all.page params[:page]
  end

  def show
    @medal = Medal.find params[:id]
  end

  def new
    @medal = Medal.new
    @achievements = Achievement.all
    @categories = Category.all
    @tags = Tag.all
  end

  def create
    @medal = Medal.new medal_params

    if @medal.save
      flash[:notice] = 'Medal created'
      redirect_to dashboard_medals_path
    else
      flash.now[:warning] = 'There were problems when trying to create a new Medal'
      render :action => :new
    end
  end

  def edit
    @medal = Medal.find params[:id]
    @achievements = Achievement.all
    @categories = Category.all
    @tags = Tag.all
  end

  def update
    @medal = Medal.find params[:id]

    if @medal.update_attributes medal_params
      flash[:notice] = 'medal has been updated'
      redirect_to dashboard_medals_path
    else
      flash.now[:warning] = 'There were problems when trying to update this Medal'
      render :action => :show
    end
  end

  def destroy
    @medal = Medal.find params[:id]

    @medal.destroy
    flash[:notice] = 'Achievement has been deleted'
    redirect_to dashboard_medals_path
  end

  private
  def medal_params
    params.require(:medal).permit(
        :title,
        :slug,
        :description,
        :medal_icon,
        :medal_icon_file_name,
        { achievement_ids:[] },
        { category_ids:[] },
        { tag_ids:[] }
    )
  end
end
