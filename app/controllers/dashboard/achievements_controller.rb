class Dashboard::AchievementsController < ApplicationController

  include Dashboard

  def index
    @achievements = Achievement.all.order(id: :desc).page params[:page]
  end

  def show
    @achievement = Achievement.find params[:id]
  end

  def new
    @achievement = Achievement.new
    @categories = Category.all
    @tags = Tag.all
  end

  def create
    @categories = Category.all
    @tags = Tag.all
    @achievement = Achievement.new achievement_params

    if @achievement.save
      flash[:notice] = 'Achievement created'
      redirect_to dashboard_achievements_path
    else
      flash.now[:warning] = 'There were problems when trying to create a new Achievement'
      render :action => :new
    end
  end

  def edit
    @achievement = Achievement.find params[:id]
    @categories = Category.all
    @tags = Tag.all
  end

  def update
    @achievement = Achievement.find params[:id]

    if @achievement.update_attributes achievement_params
      flash[:notice] = 'Achievement has been updated'
      redirect_to dashboard_achievements_path
    else
      flash.now[:warning] = 'There were problems when trying to update this achievement'
      render :action => :show
    end
  end

  def destroy
    @achievement = Achievement.find params[:id]

    @achievement.destroy
    flash[:notice] = 'Achievement has been deleted'
    redirect_to dashboard_achievements_path
  end

  private
  def achievement_params
    params.require(:achievement).permit(
                                  :title,
                                  :slug,
                                  :description,
                                  :achievement_icon,
                                  :achievement_icon_file_name,
                                  { category_ids:[] },
                                  { tag_ids:[] }
                                  )
  end

end
