class CallbacksController < Devise::OmniauthCallbacksController

  def github
    login
  end

  def facebook
    login
  end

  def vkontakte
    login
  end

  def twitter
    login
  end


  private
  def login
    @user = User.from_omniauth(request.env['omniauth.auth'])
    sign_in_and_redirect @user
  end

end
