class Users::AchievementsController < ApplicationController
  def index
    @user = User.find params[:user_id]
    @achievements = @user.achievements
                    .joins(:user_achieveds).distinct
                    .order('user_achieveds.created_at DESC')
                    .page params[:page]
  end

  def show
    @user = User.find params[:user_id]
    @achievement = @user.achievements.find params[:id]
  end
end
