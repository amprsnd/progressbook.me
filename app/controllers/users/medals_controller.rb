class Users::MedalsController < ApplicationController
  def index
    @user = User.find params[:user_id]
    @medals = @user.medals
                    .joins(:user_medaleds).distinct
                    .order('user_medaleds.created_at DESC')
                    .page params[:page]
  end

  def show
    @user = User.find params[:user_id]
    @medal = @user.medals.find params[:id]
  end
end
