class Users::ChallengesController < ApplicationController
  def index
    @user = User.find params[:user_id]
    @challenges = @user.challenges.page params[:page]
  end

  def show
    @user = User.find params[:user_id]
    @challenge = @user.challenges.find params[:id]
  end
end
