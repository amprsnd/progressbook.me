class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?

  #developing mode
  #http_basic_authenticate_with name: 'dev', password: 'fullaccess'


  #for devise
  if controller_name == 'devise'
    layout 'devise'
  end
  #protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up).push(:name)
  end


  #after login/logout paths
  def after_sign_in_path_for(resource)
    profile_path
  end

  def after_sign_out_path_for(resource)
    root_path
  end


  #for dashboard
  private
  def check_admin

    if current_user == nil || !current_user.admin?
      render file: "#{Rails.root}/public/404.html",
             status: 404,
             layout: false
    end

  end

end
