class ChallengesController < ApplicationController

  before_action :authenticate_user!, except: [:index, :show]
  layout 'no_sidebar', except: :index

  def index

    @user = current_user

    @cats = Category.all


    # TODO: not good... do refactor!
    cat_id = params[:cat]

    if @cats.exists? cat_id

      @challenges = Category.find(cat_id)
        .challenges
        .order(id: :desc)
        .page params[:page]

      @cat_id_title = Category.find(cat_id).title

    else

      @challenges = Challenge.all
        .order(id: :desc)
        .page params[:page]
      @cat_id_title = ''
      @tag_id_title = ''

    end
    # not good end

    render layout: 'cats_tags'

  end

  def show
    @user = current_user
    @challenge = Challenge.find params[:id]
  end

  def new
    @user = current_user
    @challenge = Challenge.new
  end

  def create
    @user = current_user
    @challenge = Challenge.new challenge_params
    @challenge.author_id = @user.id
    @challenge.status = 'not verified'
    @challenge.good = 0
    @challenge.bad = 0
    if @challenge.save
      flash[:notice] = 'all ok!'
      #redirect_to challenge_path(@challenge.id)
    else
      flash[:notice] = 'Something wrong...'
      render action: :new
    end

  end

  def edit
    @user = current_user
    @challenge = Challenge.find params[:id]
  end

  def update
    @challenge = Challenge.find params[:id]
  end

  def destroy
  end

  private
  def challenge_params
    params.require(:challenge).permit(:title, :description, :category_id, :days_for_complete)
  end

end
