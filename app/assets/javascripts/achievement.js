/**
 * Created by ampersand on 2/7/16.
 */

$(function () {

    //Achievement proof section
    //Choose proof type

    var image = $('.input-field.image_proof'),
        text = $('.achievement_user_achieveds_proof.text'),
        textForm = $('.achievement_user_achieveds_proof.text textarea'),
        link = $('.achievement_user_achieveds_proof.string'),
        linkForm = $('.achievement_user_achieveds_proof.string input'),
        selectors = '.achievement_user_achieveds_proof_type .select-wrapper li';

    text.css('display', 'none');
    link.css('display', 'none');
    textForm.attr('name', '');
    linkForm.attr('name', '');

    $('body').on('click', selectors, function() {

        var selectedType = $('#achievement_user_achieveds_proof_type option:selected').text();

        if        (selectedType == 'Image') {

            image.css('display', 'block');
            text.css('display', 'none');
            link.css('display', 'none');

            console.log(selectedType);


        } else if (selectedType == 'Text') {

            //

            image.css('display', 'none');
            text.css('display', 'block');
            link.css('display', 'none');

            textForm.attr('name', 'achievement[user_achieveds][proof]');
            linkForm.attr('name', '');

            console.log(selectedType);


        } else if (selectedType == 'Link') {

            image.css('display', 'none');
            text.css('display', 'none');
            link.css('display', 'block');

            textForm.attr('name', '');
            linkForm.attr('name', 'achievement[user_achieveds][proof]');

            console.log(selectedType);


        } else {}



    });



});
