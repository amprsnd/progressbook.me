$(function () {

    //init modal
    $('.modal-trigger').leanModal();

    //Init dropdown
    $('select').material_select();

    //Init tooltips
    $('.tooltipped').tooltip({delay: 50});

    //Init collapsible
    $('.collapsible').collapsible({
        accordion : false
    });

    //DatePicker
    function marginCalendar() {
        var instance = $('.pmu-instance'),
            instanceHeight = instance.height(),
            windowHeight = $(window).height();

        instance.css('margin-top', (windowHeight / 2) - (instanceHeight /2) + 'px');
    }

    //Init date picker
    $('.datepicker').pickmeup({
        format  : 'Y-m-d',
        first_day: 1,
        min: '1940-01-01',
        max: '2002-12-31',
        position: 'center',
        show: function() {
            marginCalendar();
        },
        fill: function() {
            $('#pmu-clear').on('click', function(e){
                $('.datepicker').val('');
                e.preventDefault();

            });

            $('#pmu-close').on('click', function(e){
                $('.datepicker').pickmeup('hide');
                e.preventDefault();
            });
        }
    });

    $( window ).resize(function() {
        marginCalendar();
    });


    //notices
    var Message = $('.system-message');

    //Show notice message
    Message.css({
        opacity : 1,
        top : 0
    });

    //autohide
    //TODO: if error - disable autohide
    if (Message.length != 0) {
        setTimeout(function(){
            Message.css('opacity', '0');
            setTimeout(function(){
                Message.remove();
            }, 501);
        }, 3500);
    }

    //close notice
    $('.close-this').on('click', function() {
        var closeButton = $(this);
        closeButton.parent().css('opacity', '0');
        setTimeout(function(){
            closeButton.parent().remove();
        }, 501);
    });

    //paginate fix
    $('.pagination li.disabled a').on('click', function(e){
        e.preventDefault();
    })

});
