// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery.js
//= require jquery.turbolinks
//= require jquery_ujs
//= require turbolinks
//= require_tree .

//WTF? It`s normal? 0_o
//= require_tree ../../../vendor/assets/javascripts/materialize/.

$(document).ready(function () {


});


$(function () {

    //Header menu
    $(".dropdown-button").dropdown();

    //notices
    var Message = $('.system-message');

    //Show notice message
    Message.css({
        opacity : 1,
        top : 0
    });

    //autohide
    //TODO: if error - disable autohide
    if (Message.length != 0) {
        setTimeout(function(){
            Message.css('opacity', '0');
            setTimeout(function(){
                Message.remove();
            }, 501);
        }, 3500);
    }

    //close notice
    $('.close-this').on('click', function() {
        var closeButton = $(this);
        closeButton.parent().css('opacity', '0');
        setTimeout(function(){
            closeButton.parent().remove();
        }, 501);
    })

});

