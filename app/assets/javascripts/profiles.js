$(function () {

    //Tabs standart height
    var tabHeight = 0;
    $.each($('.tab-content'), function() {

        if ($(this).height() > tabHeight) {

            tabHeight = $(this).height();
            $('.tab-content').css('height', tabHeight);

        }

    });

    if ($('.userpic-preview img').attr('src') == '/images/userpic.svg') {
        $('.userpic-preview .delete').remove();
    }

    //delete userpic
    $('body').on('click', '#delete-userpic', function(e) {
        $.ajax({
            type: 'POST',
            url: '/profile',
            data: {
                params: {
                    delete: true
                }
            },
            success: function() {
                $('.userpic-preview img').attr('src', '/images/userpic.svg');
                $('aside .card-image img').attr('src', '/images/userpic.svg');
                $('.userpic-preview .delete').remove();
                Materialize.toast('Userpic has been deleted', 4000)
            }
        });
        e.preventDefault();
    });


});
