class Achievement < ActiveRecord::Base

  #validates
  validates :title, :slug, :description, :achievement_icon,
            presence: { message: 'Sorry, can`t be blank' }
  #TODO: display error
  validates :category_ids,
            presence: { message: 'Category can`t be blank' }

  #relations
  has_many :user_achieveds, dependent: :destroy
  has_many :users, through: :user_achieveds, dependent: :destroy
  has_and_belongs_to_many :medals
  has_and_belongs_to_many :categories
  has_and_belongs_to_many :tags

  #image
  has_attached_file :achievement_icon,
                    styles: {
                        giant:  '750x750>',
                        large:  '500x500>',
                        medium: '250x250>',
                        thumb:  '100x100>',
                        small:  '50x50>'
                    },
                    default_url: '/images/default.png'
  validates_attachment_content_type :achievement_icon, content_type: /\Aimage\/.*\Z/


end
