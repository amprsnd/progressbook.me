class Tag < ActiveRecord::Base

  #validations
  validates :name, presence: { message: 'Oops, can`t be blank' }
  validates :name, length: {
                      minimum: 2,
                      maximum: 128,
                      too_short: 'Title is too short...',
                      too_long:  'Title is too long...'
                  }
  validates :name, uniqueness: { message: 'You already have this label' }

  #relations
  has_and_belongs_to_many :achievements
  has_and_belongs_to_many :medals

end
