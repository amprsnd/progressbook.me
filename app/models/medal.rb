class Medal < ActiveRecord::Base

  #validates
  validates :title, :slug, :description, :medal_icon,
            presence: { message: 'Sorry, can`t be blank' }
  validates :category_ids, :achievement_ids,
            presence: { message: 'Category and Achievements can`t be blank' }

  #relations
  has_many :user_medaleds, dependent: :destroy
  has_many :users, through: :user_medaleds, dependent: :destroy
  has_and_belongs_to_many :achievements, dependent: :destroy
  has_and_belongs_to_many :categories
  has_and_belongs_to_many :tags

  #image
  has_attached_file :medal_icon,
                    styles: {
                        giant:  '750x750>',
                        large:  '500x500>',
                        medium: '250x250>',
                        thumb:  '100x100>',
                        small:  '50x50>'
                    },
                    default_url: '/images/default.png'
  validates_attachment_content_type :medal_icon, content_type: /\Aimage\/.*\Z/

end
