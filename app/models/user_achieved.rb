class UserAchieved < ActiveRecord::Base

  #validations

  #relations
  belongs_to :user
  belongs_to :achievement

  #proof files
  #image
  has_attached_file :proof_image,
                    styles: {
                        large:  '500x500>',
                        medium: '250x250>',
                        thumb:  '100x100>'
                    },
                    default_url: '/images/default.png'
  validates_attachment_content_type :proof_image, content_type: /^image\/(png|gif|jpeg)/,
                                    message: 'only (png/gif/jpeg) images'

end
