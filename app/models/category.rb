class Category < ActiveRecord::Base

  #validations
  validates :title, :slug, :category_icon, presence: { message: 'Sorry, can`t be blank' }
  validates :title, length: {
                        minimum: 2,
                        maximum: 128,
                        too_short: 'Title is too short...',
                        too_long:  'Title is too long...'
                    }
  validates :slug, length: {
                      minimum: 2,
                      maximum: 128,
                      too_short: 'Slug is too short...',
                      too_long:  'Slug is too long...'
                  }
  validates :slug, uniqueness: { message: 'You already have this slug' }

  #relations
  has_and_belongs_to_many :achievements
  has_and_belongs_to_many :medals
  has_many :challenges

  #image
  has_attached_file :category_icon,
                    styles: {
                        normal:  '100x100>',
                        small:  '50x50>'
                    },
                    default_url: '/images/default.png'
  validates_attachment_content_type :category_icon, content_type: /\Aimage\/.*\Z/
end
