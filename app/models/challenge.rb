class Challenge < ActiveRecord::Base

  #validations
  validates :title, presence: { message: 'Sorry, can`t be blank' }
  validates :title, length: {
                        minimum: 3,
                        maximum: 128,
                        too_short: 'Title is too short...',
                        too_long:  'Title is too long...'
                    }
  validates :description, length: {
                      maximum: 255,
                      too_long:  'Description is too long...'
                  }
  validates :days_for_complete, numericality: { only_integer: true }
  validates_associated :category

  #relations
  belongs_to :category

  #files

end
