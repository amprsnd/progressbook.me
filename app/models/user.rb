class User < ActiveRecord::Base

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         :omniauthable, :omniauth_providers => [:github, :facebook, :twitter, :vkontakte]


  #validations
  validates :email, uniqueness: true
  validates :name, presence: { message: 'Can`t be blank' }
  validates :name, length: {
                      minimum: 3,
                      maximum: 128,
                      too_short: 'Name is too short...',
                      too_long:  'Name is too long...'
                  }
  validates :city, :social_link_fb, :social_link_tw, :social_link_vk, :social_link_inst, length: {
                     maximum: 128,
                     too_long:  'Sorry, too much symbols...'
                 }

  #relations

  #Achievements
  has_many :user_achieveds
  has_many :achievements, through: :user_achieveds

  #Medals
  has_many :user_medaleds
  has_many :medals, through: :user_medaleds


  #userpic
  has_attached_file :userpic,
                    styles: {
                        large:      ['x750>', :jpg],
                        medium:     ['x500>', :jpg],
                        small:      ['250x250#', :jpg],
                        thumb:      ['100x100#', :jpg]
                    },
                    convert_options: { all: '-quality 70' },
                    default_url: '/images/userpic.svg'
  validates_attachment_content_type :userpic, content_type: /\Aimage\/.*\Z/


  #OmniAuth
  def self.from_omniauth(auth)

    @member = self.find_by email: auth.info.email

    if @member.nil?

      where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
        user.provider = auth.provider
        user.uid = auth.uid
        user.name = auth.info.name
        user.email = (auth.info.email.nil? || auth.info.email == 'null' ? "#{SecureRandom.hex(8)}@progressbook.me" : auth.info.email)
        user.password = Devise.friendly_token[0,20]
        user.userpic = auth.info.image
      end

    else

      @member.provider = auth.provider
      @member.uid = auth.uid
      if @member.userpic_file_name.nil?
        @member.userpic = auth.info.image
      end
      @member.save

      return @member

    end

  end

  #country codes to names
  def country_name
    c = ISO3166::Country[country]
    c.translations[I18n.locale.to_s] || c.name
  end




end
