# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160329084120) do

  create_table "achievements", force: :cascade do |t|
    t.string   "title",                         limit: 255
    t.string   "slug",                          limit: 255
    t.string   "description",                   limit: 255
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "achievement_icon_file_name",    limit: 255
    t.string   "achievement_icon_content_type", limit: 255
    t.integer  "achievement_icon_file_size",    limit: 4
    t.datetime "achievement_icon_updated_at"
  end

  create_table "achievements_categories", id: false, force: :cascade do |t|
    t.integer "achievement_id", limit: 4, null: false
    t.integer "category_id",    limit: 4, null: false
  end

  add_index "achievements_categories", ["achievement_id", "category_id"], name: "index_achievements_categories_on_achievement_id_and_category_id", using: :btree
  add_index "achievements_categories", ["category_id", "achievement_id"], name: "index_achievements_categories_on_category_id_and_achievement_id", using: :btree

  create_table "achievements_medals", id: false, force: :cascade do |t|
    t.integer "medal_id",       limit: 4, null: false
    t.integer "achievement_id", limit: 4, null: false
  end

  create_table "achievements_tags", id: false, force: :cascade do |t|
    t.integer "achievement_id", limit: 4, null: false
    t.integer "tag_id",         limit: 4, null: false
  end

  add_index "achievements_tags", ["achievement_id", "tag_id"], name: "index_achievements_tags_on_achievement_id_and_tag_id", using: :btree
  add_index "achievements_tags", ["tag_id", "achievement_id"], name: "index_achievements_tags_on_tag_id_and_achievement_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "title",                      limit: 255
    t.string   "description",                limit: 255
    t.string   "icon",                       limit: 255
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "slug",                       limit: 255
    t.string   "category_icon_file_name",    limit: 255
    t.string   "category_icon_content_type", limit: 255
    t.integer  "category_icon_file_size",    limit: 4
    t.datetime "category_icon_updated_at"
  end

  create_table "categories_medals", id: false, force: :cascade do |t|
    t.integer "medal_id",    limit: 4, null: false
    t.integer "category_id", limit: 4, null: false
  end

  add_index "categories_medals", ["category_id", "medal_id"], name: "index_categories_medals_on_category_id_and_medal_id", using: :btree
  add_index "categories_medals", ["medal_id", "category_id"], name: "index_categories_medals_on_medal_id_and_category_id", using: :btree

  create_table "challenges", force: :cascade do |t|
    t.string   "title",             limit: 255
    t.string   "description",       limit: 255
    t.datetime "ended_at"
    t.integer  "days_for_complete", limit: 4
    t.string   "status",            limit: 255
    t.integer  "author_id",         limit: 4
    t.integer  "good",              limit: 4
    t.integer  "bad",               limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "category_id",       limit: 4
  end

  add_index "challenges", ["author_id"], name: "index_challenges_on_author_id", using: :btree
  add_index "challenges", ["category_id"], name: "index_challenges_on_category_id", using: :btree

  create_table "medals", force: :cascade do |t|
    t.string   "title",                   limit: 255
    t.string   "description",             limit: 255
    t.string   "slug",                    limit: 255
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "medal_icon_file_name",    limit: 255
    t.string   "medal_icon_content_type", limit: 255
    t.integer  "medal_icon_file_size",    limit: 4
    t.datetime "medal_icon_updated_at"
  end

  create_table "medals_tags", id: false, force: :cascade do |t|
    t.integer "medal_id", limit: 4, null: false
    t.integer "tag_id",   limit: 4, null: false
  end

  add_index "medals_tags", ["medal_id", "tag_id"], name: "index_medals_tags_on_medal_id_and_tag_id", using: :btree
  add_index "medals_tags", ["tag_id", "medal_id"], name: "index_medals_tags_on_tag_id_and_medal_id", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "description", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "user_achieveds", force: :cascade do |t|
    t.boolean  "have_proof",               limit: 1
    t.string   "proof_type",               limit: 255
    t.integer  "good_proof",               limit: 4
    t.integer  "user_id",                  limit: 4
    t.integer  "achievement_id",           limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "proof",                    limit: 255
    t.string   "proof_image_file_name",    limit: 255
    t.string   "proof_image_content_type", limit: 255
    t.integer  "proof_image_file_size",    limit: 4
    t.datetime "proof_image_updated_at"
  end

  add_index "user_achieveds", ["achievement_id"], name: "index_user_achieveds_on_achievement_id", using: :btree
  add_index "user_achieveds", ["user_id"], name: "index_user_achieveds_on_user_id", using: :btree

  create_table "user_medaleds", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "medal_id",   limit: 4
    t.integer  "good_proof", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "user_medaleds", ["medal_id"], name: "index_user_medaleds_on_medal_id", using: :btree
  add_index "user_medaleds", ["user_id"], name: "index_user_medaleds_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "",    null: false
    t.string   "encrypted_password",     limit: 255, default: "",    null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.boolean  "admin",                  limit: 1,   default: false
    t.string   "name",                   limit: 255
    t.string   "provider",               limit: 255
    t.string   "uid",                    limit: 255
    t.string   "country",                limit: 255
    t.string   "city",                   limit: 255
    t.string   "bio",                    limit: 255
    t.string   "social_link_fb",         limit: 255
    t.string   "social_link_tw",         limit: 255
    t.string   "social_link_vk",         limit: 255
    t.date     "date_of_birth"
    t.string   "userpic_file_name",      limit: 255
    t.string   "userpic_content_type",   limit: 255
    t.integer  "userpic_file_size",      limit: 4
    t.datetime "userpic_updated_at"
    t.string   "gender",                 limit: 255
    t.string   "social_link_inst",       limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "challenges", "categories"
  add_foreign_key "user_medaleds", "medals"
  add_foreign_key "user_medaleds", "users"
end
