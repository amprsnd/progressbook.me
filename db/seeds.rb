# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
@ach = 3..48 #1..48
@med = 2..50
@cat = 1..24
@tag = 1..128
@usr = 1..197

@usr.each do |i|

  @rand = rand(1..16)
  @arr = []
  @rand.times do
    @arr.push(rand(@med))
  end

  @u = User.find(i)
  @u.medal_ids = @arr
end

48.times do |i|
  #achievs
  @rand = rand(2..12)
  @arr = []
  @rand.times do
    @arr.push(rand(@ach))
  end
  #cats
  @rand_cats = rand(1..3)
  @arr_cats = []
  @rand_cats.times do
    @arr_cats.push(rand(@cat))
  end
  #tags
  @rand_tags = rand(3..7)
  @arr_tags = []
  @rand_tags.times do
    @arr_tags.push(rand(@tag))
  end

   @medal = Medal.create(
       title: "Medal #{i}",
       description: "Description #{i}",
       slug: "medal-#{i}",
       medal_icon: File.new("/home/ampersand/pb/m/med_#{rand(1..15)}.jpg", 'r'),
       achievement_ids: @arr,
       category_ids: @arr_cats,
       tag_ids: @arr_tags
   )
   puts '======================================================='
   puts "ID: #{@medal.id}, cats: #{@arr_cats}, tags: #{@arr_tags}, achievs: #{@arr}"
 end

 @usr.each do |i|

   @rand = rand(3..7)
   @arr = []
   @rand.times do
     @arr.push(rand(@ach))
   end

   @u = User.find(i)
   @u.achievement_ids = @arr
 end

 @ach.each do |i|
   @a = Achievement.find(i)
   # Eat shit - not DRY! >=(
   @a.category_ids = [rand(@cat), rand(@cat), rand(@cat)]
   @a.tag_ids = [rand(@tag), rand(@tag), rand(@tag), rand(@tag), rand(@tag)]
 end


 3.times do |i|
   User.create(
     email: "fake#{i}@example.com",
     password:'password',
     password_confirmation: 'password',
     name: "Fake-#{i}",
     userpic: File.new("/home/ampersand/pb/u/up#{rand(1..10)}.jpg", 'r')
   )
 end

 48.times do |i|
    Achievement.create(
        title: "Achiev #{i}",
        description: "Description #{i}",
        slug: "achiev-#{i}",
        achievement_icon: File.new("/home/ampersand/pb/a/ach#{rand(1..15)}.jpg", 'r')
    )
  end


  24.times do |i|
    Category.create(
        title: "Cat #{i}",
        description: "Description #{i}",
        slug: "cat-#{i}",
        category_icon: File.new("/home/ampersand/pb/c/animals-#{rand(1..12)}.png", 'r')
    )
  end

  128.times do |i|
    Tag.create(
        name: "Tag №#{i}",
        description: "Description #{i}"
    )
  end
