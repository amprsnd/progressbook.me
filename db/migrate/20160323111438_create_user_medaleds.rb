class CreateUserMedaleds < ActiveRecord::Migration
  def change
    create_table :user_medaleds do |t|
      t.references :user, index: true, foreign_key: true
      t.references :medal, index: true, foreign_key: true
      t.integer :good_proof

      t.timestamps null: false
    end
  end
end
