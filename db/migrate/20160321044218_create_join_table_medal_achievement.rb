class CreateJoinTableMedalAchievement < ActiveRecord::Migration
  def change
    create_join_table :medals, :achievements do |t|
      # t.index [:medal_id, :achievement_id]
      # t.index [:achievement_id, :medal_id]
    end
  end
end
