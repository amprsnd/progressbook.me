class AddAttachmentAchievementIconToAchievements < ActiveRecord::Migration
  def self.up
    change_table :achievements do |t|
      t.attachment :achievement_icon
    end
  end

  def self.down
    remove_attachment :achievements, :achievement_icon
  end
end
