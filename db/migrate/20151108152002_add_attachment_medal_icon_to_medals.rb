class AddAttachmentMedalIconToMedals < ActiveRecord::Migration
  def self.up
    change_table :medals do |t|
      t.attachment :medal_icon
    end
  end

  def self.down
    remove_attachment :medals, :medal_icon
  end
end
