class AddAttachmentProofImageToUserAchieveds < ActiveRecord::Migration
  def self.up
    change_table :user_achieveds do |t|
      t.attachment :proof_image
    end
  end

  def self.down
    remove_attachment :user_achieveds, :proof_image
  end
end
