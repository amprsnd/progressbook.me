class CreateJoinTableAchievementCategory < ActiveRecord::Migration
  def change
    create_join_table :achievements, :categories do |t|
      t.index [:achievement_id, :category_id]
      t.index [:category_id, :achievement_id]
    end
  end
end
