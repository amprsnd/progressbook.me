class CreateJoinTableMedalTag < ActiveRecord::Migration
  def change
    create_join_table :medals, :tags do |t|
      t.index [:medal_id, :tag_id]
      t.index [:tag_id, :medal_id]
    end
  end
end
