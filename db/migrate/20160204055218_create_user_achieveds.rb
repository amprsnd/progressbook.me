class CreateUserAchieveds < ActiveRecord::Migration
  def change
    create_table :user_achieveds do |t|
      t.boolean :have_proof
      t.string :proof_type
      t.integer :good_proof
      t.belongs_to :user, index: true
      t.belongs_to :achievement, index: true

      t.timestamps null: false
    end
  end
end
