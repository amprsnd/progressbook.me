class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.string :title
      t.string :description
      t.datetime :ended_at
      t.integer :days_for_complete
      t.string :status
      t.integer :author_id, index: true
      t.integer :good
      t.integer :bad

      t.timestamps null: false
    end
  end
end
