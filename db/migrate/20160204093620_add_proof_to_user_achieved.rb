class AddProofToUserAchieved < ActiveRecord::Migration
  def change
    add_column :user_achieveds, :proof, :string, :length => 512
  end
end
