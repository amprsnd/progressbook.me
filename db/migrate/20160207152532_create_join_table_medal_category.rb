class CreateJoinTableMedalCategory < ActiveRecord::Migration
  def change
    create_join_table :medals, :categories do |t|
      t.index [:medal_id, :category_id]
      t.index [:category_id, :medal_id]
    end
  end
end
