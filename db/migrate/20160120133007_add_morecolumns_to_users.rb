class AddMorecolumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :country, :string
    add_column :users, :city, :string
    add_column :users, :bio, :string
    add_column :users, :social_link_fb, :string
    add_column :users, :social_link_tw, :string
    add_column :users, :social_link_vk, :string
    add_column :users, :date_of_birth, :date
  end
end
