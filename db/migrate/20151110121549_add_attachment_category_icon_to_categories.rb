class AddAttachmentCategoryIconToCategories < ActiveRecord::Migration
  def self.up
    change_table :categories do |t|
      t.attachment :category_icon
    end
  end

  def self.down
    remove_attachment :categories, :category_icon
  end
end
