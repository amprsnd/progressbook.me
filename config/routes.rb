Rails.application.routes.draw do

  #satatic
  root 'static#home'
  get 'about' => 'static#about'
  get 'contacts' => 'static#contacts'
  get 'presentation' => 'static#presentation'

  devise_for :users, path: 'profile', path_names: {
                       sign_in: 'login',
                       sign_out: 'logout',
                       password: 'secret',
                       confirmation: 'verification',
                       unlock: 'unblock',
                       registration: 'register',
                       sign_up: 'registration'
                   }, :controllers => { :omniauth_callbacks => 'callbacks' }

  #user area
  resource  :profile, only: [:show, :edit, :update]
  post '/profile', to: 'profiles#delete_userpic'
  resources :users, only: [:index, :show]

  resources :categories, only: [:index, :show], param: :slug
  resources :tags, only: [:index, :show]

  resources :achievements,  only: [:index, :show, :edit, :update]
  resources :medals,        only: [:index, :show, :update]
  resources :challenges
  #resources :quests, only: [:index, :show, :update]

  resources :users, only: [:index, :show] do
    resources :achievements,
              controller: 'users/achievements',
              only: [:index, :show]
    resources :medals,
              controller: 'users/medals',
              only: [:index, :show]
    resources :challenges,
              controller: 'users/challenges',
              only: [:index, :show]
  end

  #admin area
  #resources :dashboard, only: [:index]
  get 'dashboard' => 'dashboard#index'
    namespace :dashboard do

      resources :categories
      resources :tags

      #achievement
      resources :achievements

      #medal
      resources :medals

      #challenges
      resources :challenges

      resources :users

    end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
